lis = ['a', ['b', ['c', 'd'], 'e']]


def flatten(lis):
    new_lis = []
    for item in lis:
        if type(item) == type([]):
            new_lis.extend(flatten(item))
        else:
            new_lis.append(item)
    return new_lis

print (flatten(lis))